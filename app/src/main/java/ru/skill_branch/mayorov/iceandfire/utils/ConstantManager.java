package ru.skill_branch.mayorov.iceandfire.utils;

import java.util.ArrayList;

/**
 * Created by Sergey on 09.10.2016.
 */

public interface ConstantManager {
    String TAG_PREFIX = "DEV ";

    String LOADING_DATA_TAG = "loading_data";

    int STARK_HOUSE_NUMBER = 362;
    int LANNISTER_HOUSE_NUMBER = 229;
    int TARGARYEN_HOUSE_NUMBER = 378;


    String HOUSE_ARG_ID = "HOUSE_ARG_ID";
}
