package ru.skill_branch.mayorov.iceandfire.ui.activities;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.NavUtils;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.io.Serializable;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.skill_branch.mayorov.iceandfire.R;
import ru.skill_branch.mayorov.iceandfire.data.managers.DataManager;
import ru.skill_branch.mayorov.iceandfire.data.managers.models.CharacterInfo;
import ru.skill_branch.mayorov.iceandfire.mvp.presenters.abstracts.ICharacterPresenter;
import ru.skill_branch.mayorov.iceandfire.mvp.presenters.concrete.CharacterPresenter;
import ru.skill_branch.mayorov.iceandfire.mvp.views.ICharacterView;
import ru.skill_branch.mayorov.iceandfire.utils.ConstantManager;

public class CharacterActivity extends AppCompatActivity implements View.OnClickListener, ICharacterView {
    private final static String CHARACTER_INFO_EXTRA = "CHARACTER_INFO_EXTRA";

    private CharacterPresenter mPresenter = CharacterPresenter.getInstance();

    private CharacterInfo mCharacterInfo;

    //region ========== Views ============
    @BindView(R.id.words_txt)
    TextView mWords;

    @BindView(R.id.born_txt)
    TextView mBorn;

    @BindView(R.id.titles_txt)
    TextView mTitles;

    @BindView(R.id.aliases_txt)
    TextView mAliases;

    @BindView(R.id.father_btn)
    Button mFatherBtn;

    @BindView(R.id.mother_btn)
    Button mMotherBtn;

    @BindView(R.id.mother_layout)
    LinearLayout mMotherLayout;

    @BindView(R.id.father_layout)
    LinearLayout mFatherLayout;


    @BindView(R.id.house_photo_img)
    ImageView mHouseImage;

    @BindView(R.id.coordinator_layout)
    CoordinatorLayout mCoordinatorLayout;

    @BindView(R.id.collapsing_toolbar)
    CollapsingToolbarLayout mCollapsingToolbarLayout;

    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    //endregion

    //region ========== Life Cycle =========
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_character);
        ButterKnife.bind(this);

        mCharacterInfo = (CharacterInfo) getIntent().getSerializableExtra(CHARACTER_INFO_EXTRA);
        mPresenter.takeView(this);
        mPresenter.init(mCharacterInfo);

        initViews();
        setupToolbar();
    }

    @Override
    protected void onDestroy() {
        mPresenter.dropView();
        super.onDestroy();
    }
    //endregion

    private void initViews() {
        mFatherBtn.setOnClickListener(this);
        mMotherBtn.setOnClickListener(this);

        if (mCharacterInfo != null) {
            mCollapsingToolbarLayout.setTitle(mCharacterInfo.getName());
            mWords.setText(mCharacterInfo.getWords());
            mBorn.setText(mCharacterInfo.getBorn());
            mTitles.setText(mCharacterInfo.getTitles());
            mAliases.setText(mCharacterInfo.getAliases());

            mFatherBtn.setText(mCharacterInfo.getFatherName());
            mMotherBtn.setText(mCharacterInfo.getMotherName());
        }
    }

    private void setupToolbar() {
        setSupportActionBar(mToolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            getPresenter().onHomeButtonClick();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.mother_btn) {
            getPresenter().onMotherButtonClick(mCharacterInfo);
        } else {
            getPresenter().onFatherButtonClick(mCharacterInfo);
        }
    }

    //region ============ ICharacterView ============
    @Override
    public void showMessage(String message) {
        Snackbar.make(mCoordinatorLayout, message, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public ICharacterPresenter getPresenter() {
        return mPresenter;
    }

    @Override
    public void startCharacterActivity(CharacterInfo characterInfo) {
        startActivity(CharacterActivity.newIntent(this, characterInfo));
    }

    @Override
    public void startCharacterListActivity() {
        NavUtils.navigateUpFromSameTask(this);
    }

    @Override
    public void hideFather() {
        mFatherLayout.setVisibility(View.GONE);
    }

    @Override
    public void hideMother() {
        mMotherLayout.setVisibility(View.GONE);
    }

    @Override
    public void loadImage(int resourceId) {
        //TODO сделать Placeholder
        Picasso.with(this)
                .load(resourceId)
                .fit()
                .into(mHouseImage);
    }
    //endregion

    public static Intent newIntent(Context context, Serializable serializable) {
        Intent intent = new Intent(context, CharacterActivity.class);
        intent.putExtra(CHARACTER_INFO_EXTRA, serializable);
        return intent;
    }
}
