package ru.skill_branch.mayorov.iceandfire.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by Sergey on 23.10.2016.
 */

public class NetworkStatusChecker {
    public static boolean isNetworkAvailable(){
        Context context = StartApplication.getContext();

        ConnectivityManager cm = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo  activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
    }

}
