package ru.skill_branch.mayorov.iceandfire.data.network.api;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import ru.skill_branch.mayorov.iceandfire.data.network.res.CharacterModelRes;
import ru.skill_branch.mayorov.iceandfire.data.network.res.HouseModelRes;

/**
 * Created by Sergey on 15.10.2016.
 */

public interface RestService {
    @GET("houses/{id}")
    Call<HouseModelRes> getHouseInfo(@Path("id") int houseId);

    @GET("characters/{id}")
    Call<CharacterModelRes> getCharacter(@Path("id") int characterId);


}
