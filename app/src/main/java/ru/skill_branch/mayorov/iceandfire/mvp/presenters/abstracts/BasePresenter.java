package ru.skill_branch.mayorov.iceandfire.mvp.presenters.abstracts;


import android.support.annotation.Nullable;



public abstract class BasePresenter<T> {
    private T mView;

    public void takeView(T view){
        mView = view;
    }

    public void dropView(){
        mView = null;
    }

    @Nullable
    public T getView(){
        return mView;
    }
}
