package ru.skill_branch.mayorov.iceandfire.mvp.presenters.abstracts;


import ru.skill_branch.mayorov.iceandfire.data.managers.models.CharacterInfo;

public interface ICharacterPresenter  {
    void init(CharacterInfo characterInfo);

    void onMotherButtonClick(CharacterInfo characterInfo);

    void onFatherButtonClick(CharacterInfo characterInfo);

    void onHomeButtonClick();
}
