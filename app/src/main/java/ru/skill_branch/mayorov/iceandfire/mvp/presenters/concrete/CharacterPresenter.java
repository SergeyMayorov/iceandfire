package ru.skill_branch.mayorov.iceandfire.mvp.presenters.concrete;


import ru.skill_branch.mayorov.iceandfire.R;
import ru.skill_branch.mayorov.iceandfire.data.managers.DataManager;
import ru.skill_branch.mayorov.iceandfire.data.managers.models.CharacterInfo;
import ru.skill_branch.mayorov.iceandfire.mvp.models.CharacterModel;
import ru.skill_branch.mayorov.iceandfire.mvp.presenters.abstracts.BasePresenter;
import ru.skill_branch.mayorov.iceandfire.mvp.presenters.abstracts.ICharacterPresenter;
import ru.skill_branch.mayorov.iceandfire.mvp.views.ICharacterView;
import ru.skill_branch.mayorov.iceandfire.utils.ConstantManager;


public class CharacterPresenter extends BasePresenter<ICharacterView> implements ICharacterPresenter {

    private static CharacterPresenter instance = new CharacterPresenter();

    private CharacterPresenter() {
    }

    public static CharacterPresenter getInstance() {
        return instance;
    }

    @Override
    public void init(CharacterInfo characterInfo) {
        if (getView() != null && characterInfo != null) {
            if (characterInfo.getDied()) {
                getView().showMessage(characterInfo.getName() + " is dead");
            }

            if (characterInfo.getRemoteFatherId() == null || characterInfo.getFatherName() == null) {
                getView().hideFather();
            }
            if (characterInfo.getRemoteMotherId() == null || characterInfo.getMotherName() == null) {
                getView().hideMother();
            }

            int drawableResourceId = -1;
            switch (characterInfo.getRemoteHouseId()) {
                case ConstantManager.STARK_HOUSE_NUMBER:
                    drawableResourceId = R.drawable.stark;
                    break;
                case ConstantManager.LANNISTER_HOUSE_NUMBER:
                    drawableResourceId = R.drawable.lannister;
                    break;
                case ConstantManager.TARGARYEN_HOUSE_NUMBER:
                    drawableResourceId = R.drawable.targarien;
                    break;
            }

            if(drawableResourceId != -1){
                getView().loadImage(drawableResourceId);
            }

        }
    }

    @Override
    public void onMotherButtonClick(CharacterInfo characterInfo) {
        Integer id = characterInfo.getRemoteMotherId();
        onButtonClick(id);
    }

    @Override
    public void onFatherButtonClick(CharacterInfo characterInfo) {
        Integer id = characterInfo.getRemoteFatherId();
        onButtonClick(id);
    }

    @Override
    public void onHomeButtonClick() {
        if(getView()!=null){
            getView().startCharacterListActivity();
        }
    }

    private void onButtonClick(Integer id) {
        CharacterInfo characterInfo = new CharacterModel().getCharacterInfo(id);

        if (characterInfo != null && getView() != null) {
            getView().startCharacterActivity(characterInfo);
        }
    }
}
