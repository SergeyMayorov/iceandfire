package ru.skill_branch.mayorov.iceandfire.ui;

/**
 * Created by Sergey on 15.10.2016.
 */

public interface AppConfig {
    String BASE_URL = "http://www.anapioficeandfire.com/api/";
    String CHARACTER_URL = AppConfig.BASE_URL+"characters/";
    int TIMEOUT = 3;
}
