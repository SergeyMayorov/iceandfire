package ru.skill_branch.mayorov.iceandfire.utils;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import org.greenrobot.greendao.database.Database;

import ru.skill_branch.mayorov.iceandfire.data.storage.models.DaoMaster;
import ru.skill_branch.mayorov.iceandfire.data.storage.models.DaoSession;

public class StartApplication extends Application {

    private static Context sContext;
    private static DaoSession sDaoSession;
    private static SharedPreferences sSharedPreferences;



    @Override

    public void onCreate() {
        super.onCreate();
        sContext = getApplicationContext();
        sSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(this, "iceandfire-db");
        Database db = helper.getWritableDb();
        sDaoSession = new DaoMaster(db).newSession();
    }

    public static SharedPreferences getSharedPreferences() {
        return sSharedPreferences;
    }

    public static Context getContext(){
        return StartApplication.sContext;
    }

    public static DaoSession getDaoSession() {
        return sDaoSession;
    }
}
