package ru.skill_branch.mayorov.iceandfire.mvp.presenters.abstracts;

import android.view.MenuItem;

public interface ICharacterListPresenter  {
    void onNavigationHomeButtonClick();

    void onNavigationDrawerItemSelected(MenuItem item);

    void onTabSelected(int tabPosition);
}
