package ru.skill_branch.mayorov.iceandfire.data.managers.models;

import java.io.Serializable;

import ru.skill_branch.mayorov.iceandfire.data.storage.models.Character;

public class CharacterInfo implements Serializable {

    public CharacterInfo(String name,
                         String titles,
                         String aliases,
                         String born,
                         boolean isDead,
                         Integer remoteMotherId,
                         String motherName,
                         Integer remoteFatherId,
                         String fatherName,
                         String words,
                         Integer houseId){

        mName = name;
        mTitles = titles;
        mAliases = aliases;
        mBorn = born;
        mDied = isDead;
        mRemoteMotherId = remoteMotherId;
        mMotherName = motherName;
        mRemoteFatherId = remoteFatherId;
        mFatherName = fatherName;
        mWords = words;
        mRemoteHouseId = houseId;

    }

    private String mName;

    private String mWords;

    private String mTitles;

    private String mAliases;

    private String mBorn;

    private boolean mDied;

    private String mMotherName;

    private String mFatherName;

    private Integer mRemoteMotherId;

    private Integer mRemoteFatherId;

    private Integer mRemoteHouseId;

    public String getName() {
        return mName;
    }

    public String getWords() {
        return mWords;
    }

    public String getTitles() {
        return mTitles;
    }

    public String getAliases() {
        return mAliases;
    }

    public String getBorn() {
        return mBorn;
    }

    public boolean getDied() {
        return mDied;
    }

    public Integer getRemoteMotherId() {
        return mRemoteMotherId;
    }

    public Integer getRemoteFatherId() {
        return mRemoteFatherId;
    }

    public String getMotherName() {
        return mMotherName;
    }

    public String getFatherName() {
        return mFatherName;
    }

    public Integer getRemoteHouseId() {
        return mRemoteHouseId;
    }
}
