package ru.skill_branch.mayorov.iceandfire.data.network;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.redmadrobot.chronos.ChronosOperation;
import com.redmadrobot.chronos.ChronosOperationResult;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;
import ru.skill_branch.mayorov.iceandfire.data.managers.DataManager;
import ru.skill_branch.mayorov.iceandfire.data.network.res.CharacterModelRes;
import ru.skill_branch.mayorov.iceandfire.data.network.res.HouseModelRes;
import ru.skill_branch.mayorov.iceandfire.data.storage.models.Character;
import ru.skill_branch.mayorov.iceandfire.data.storage.models.CharacterDao;
import ru.skill_branch.mayorov.iceandfire.data.storage.models.House;
import ru.skill_branch.mayorov.iceandfire.data.storage.models.HouseDao;
import ru.skill_branch.mayorov.iceandfire.ui.AppConfig;
import ru.skill_branch.mayorov.iceandfire.utils.ConstantManager;
import ru.skill_branch.mayorov.iceandfire.utils.NetworkStatusChecker;
import ru.skill_branch.mayorov.iceandfire.utils.StatusWrapperResult;

public class DataLoaderOperation extends ChronosOperation<StatusWrapperResult> {
    private DataManager mDataManager;
    private HouseDao mHouseDao;
    private CharacterDao mCharacterDao;

    private Date mStartDate;

    private ArrayList<Integer> mHouseNumbers;


    public DataLoaderOperation() {
        mDataManager = DataManager.getInstance();

        mHouseDao = mDataManager.getDaoSession().getHouseDao();
        mCharacterDao = mDataManager.getDaoSession().getCharacterDao();
        mStartDate = new Date();

        mHouseNumbers = new ArrayList<>();
        mHouseNumbers.add(ConstantManager.STARK_HOUSE_NUMBER);
        mHouseNumbers.add(ConstantManager.LANNISTER_HOUSE_NUMBER);
        mHouseNumbers.add(ConstantManager.TARGARYEN_HOUSE_NUMBER);

    }

    @Nullable
    @Override
    public StatusWrapperResult run() {

        try {
            for(Integer houseId : mHouseNumbers) {
                if (!DataManager.getInstance().isHouseDownloaded(houseId)) {
                    if(NetworkStatusChecker.isNetworkAvailable()) {
                        loadData(houseId);
                    }
                    else{
                        return new StatusWrapperResult(false, "Нет связи с сервером");
                    }
                }
            }

            long seconds = (new Date().getTime() - mStartDate.getTime()) / 1000;

            if (seconds < AppConfig.TIMEOUT) {
                Thread.sleep((AppConfig.TIMEOUT - seconds) * 1000);
            }

        } catch (Exception ex) {
            return new StatusWrapperResult(false, ex.getMessage());
        }
        return new StatusWrapperResult(true);
    }

    private void loadData(int houseId) throws IOException {
            HouseModelRes houseRes = getHouseInfoFromNetwork(houseId);
            final House house = new House(houseRes, houseId);
            final List<Character> characters = getCharacterListFromNetwork(houseRes.getSwornMembers(), houseId);

            mDataManager.getDaoSession().runInTx(new Runnable() {
                @Override
                public void run() {
                    mHouseDao.insertOrReplace(house);
                    mCharacterDao.insertOrReplaceInTx(characters);
                }
            });
    }


    private HouseModelRes getHouseInfoFromNetwork(int houseId) throws IOException {
        Call<HouseModelRes> callHouse = mDataManager.getHouseInfoFromNetwork(houseId);
        Response<HouseModelRes> responseHouse = callHouse.execute();
        if (responseHouse.code() == HttpURLConnection.HTTP_OK) {
            return responseHouse.body();
        } else {
            throw new IOException(responseHouse.message());
        }
    }

    private List<Character> getCharacterListFromNetwork(List<String> swornMembers, int houseId) throws IOException {
        List<Character> characters = new ArrayList<>(swornMembers.size());
        for (String url : swornMembers) {
            Integer characterId = getCharacterIdFromUrl(url);
            if (characterId != null) {
                Call<CharacterModelRes> callCharacter = mDataManager.getCharacterFromNetwork(characterId);
                Response<CharacterModelRes> responseCharacter = callCharacter.execute();
                if (responseCharacter.code() == HttpURLConnection.HTTP_OK) {
                    CharacterModelRes model = responseCharacter.body();
                    Integer fatherId = getCharacterIdFromUrl(model.getFather());
                    Integer motherId = getCharacterIdFromUrl(model.getMother());

                    String aliases = joinStrings(model.getAliases());
                    String titles = joinStrings(model.getTitles());
                    characters.add(new Character(responseCharacter.body(), characterId, houseId,
                            fatherId, motherId, aliases, titles));
                }
            }
        }
        return characters;
    }


    private Integer getCharacterIdFromUrl(String characterUrl) {
        if (characterUrl.contains(AppConfig.CHARACTER_URL)) {
            return Integer.parseInt(characterUrl.replace(AppConfig.CHARACTER_URL, ""));
        } else {
            return null;
        }
    }

    private String joinStrings(List<String> list) {
        StringBuilder sb = new StringBuilder();
        int index;
        for (index = 0; index < list.size() - 1; index++) {
            sb.append(list.get(index));
            sb.append("\n");
        }
        if (list.size() > index) {
            sb.append(list.get(index));
        }
        return sb.toString();
    }


    @NonNull
    @Override
    public Class<? extends ChronosOperationResult<StatusWrapperResult>> getResultClass() {
        return Result.class;
    }

    public final static class Result extends ChronosOperationResult<StatusWrapperResult> {
    }
}