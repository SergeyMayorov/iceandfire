package ru.skill_branch.mayorov.iceandfire.ui.activities;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;

import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.skill_branch.mayorov.iceandfire.R;
import ru.skill_branch.mayorov.iceandfire.mvp.presenters.abstracts.ISplashPresenter;
import ru.skill_branch.mayorov.iceandfire.mvp.presenters.concrete.SplashPresenter;
import ru.skill_branch.mayorov.iceandfire.mvp.views.ISplashView;
import ru.skill_branch.mayorov.iceandfire.data.network.DataLoaderOperation;

public class SplashActivity extends CustomChronosActivity implements ISplashView {
    private SplashPresenter mPresenter = SplashPresenter.getInstance();

    private ProgressDialog mProgressDialog;

    @BindView(R.id.coordinator_layout)
    CoordinatorLayout mCoordinatorLayout;


    //region ================= Life cycle =================
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        ButterKnife.bind(this);

        mPresenter.takeView(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mPresenter.startLoadData();
    }

    @Override
    protected void onDestroy() {
        mPresenter.dropView();
        super.onDestroy();
    }
    //endregion

    //region ================= ISplashView =================
    public void showProgress() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this, R.style.custom_dialog);
            mProgressDialog.setCancelable(false);
            mProgressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            mProgressDialog.show();
            mProgressDialog.setContentView(R.layout.progress_splash);
        } else {
            mProgressDialog.show();
            mProgressDialog.setContentView(R.layout.progress_splash);
        }
    }

    public void hideProgress() {
        if (mProgressDialog != null) {
            if (mProgressDialog.isShowing()) {
                mProgressDialog.dismiss();
            }
        }
    }

    public void showMessage(String message) {
        Snackbar.make(mCoordinatorLayout, message, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void startActivity() {
        startActivity(CharacterListActivity.newIntent(SplashActivity.this));
    }

    @Override
    public void finishActivity() {
        finish();
    }

    @Override
    public ISplashPresenter getPresenter() {
        return mPresenter;
    }
    //endregion


    //Вызывается через рефлексию
    public void onOperationFinished(final DataLoaderOperation.Result result) {
       getPresenter().finishLoadData(result);
    }
}
