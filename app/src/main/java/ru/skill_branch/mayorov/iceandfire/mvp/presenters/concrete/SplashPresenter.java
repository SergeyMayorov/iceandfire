package ru.skill_branch.mayorov.iceandfire.mvp.presenters.concrete;


import ru.skill_branch.mayorov.iceandfire.data.network.DataLoaderOperation;
import ru.skill_branch.mayorov.iceandfire.mvp.presenters.abstracts.BasePresenter;
import ru.skill_branch.mayorov.iceandfire.mvp.presenters.abstracts.ISplashPresenter;
import ru.skill_branch.mayorov.iceandfire.mvp.views.ISplashView;
import ru.skill_branch.mayorov.iceandfire.utils.ConstantManager;

public class SplashPresenter extends BasePresenter<ISplashView> implements ISplashPresenter {

    private static SplashPresenter instance = new SplashPresenter();

    private SplashPresenter() {}

    public static SplashPresenter getInstance() {
        return instance;
    }


    @Override
    public void startLoadData() {
        if (getView() != null) {
            getView().showProgress();
            getView().runOperation(new DataLoaderOperation(), ConstantManager.LOADING_DATA_TAG);
        }
    }

    @Override
    public void finishLoadData(DataLoaderOperation.Result result) {
        if(getView() != null) {
            if (result.getOutput().isSuccessful()) {
                getView().hideProgress();
                getView().startActivity();
                getView().finishActivity();
            } else {
                if (result.getOutput().getMessage() != null) {
                    getView().showMessage(result.getOutput().getMessage());
                } else {
                    getView().showMessage(result.getErrorMessage());
                }
                getView().hideProgress();
            }
        }
    }
}
