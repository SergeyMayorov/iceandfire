package ru.skill_branch.mayorov.iceandfire.ui.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import ru.skill_branch.mayorov.iceandfire.R;
import ru.skill_branch.mayorov.iceandfire.data.managers.DataManager;
import ru.skill_branch.mayorov.iceandfire.data.storage.models.Character;
import ru.skill_branch.mayorov.iceandfire.utils.ConstantManager;


public class CharacterAdapter extends RecyclerView.Adapter<CharacterAdapter.CharacterViewHolder> {
    private List<Character> mCharacters;
    private int mHouseId;
    private CharacterViewHolder.CustomClickListener mCustomClickListener;

    public CharacterAdapter(List<Character> characters, int houseId,
                            CharacterViewHolder.CustomClickListener customClickListener){
        this.mCharacters = characters;
        this.mHouseId = houseId;
        this.mCustomClickListener = customClickListener;
    }


    @Override
    public CharacterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_character_list, parent, false);

        return new CharacterViewHolder(view,mCustomClickListener);
    }

    @Override
    public void onBindViewHolder(CharacterViewHolder holder, int position) {
        holder.mCharacterName.setText(mCharacters.get(position).getName());
        holder.mCharacterAliases.setText(mCharacters.get(position).getAliases());

        int drawableResourceId;
        switch (mHouseId){
            case ConstantManager.STARK_HOUSE_NUMBER:
                drawableResourceId = R.drawable.stark_icon;
                break;
            case ConstantManager.LANNISTER_HOUSE_NUMBER:
                drawableResourceId = R.drawable.lanister_icon;
                break;
            case ConstantManager.TARGARYEN_HOUSE_NUMBER:
                drawableResourceId = R.drawable.targaryen_icon;
                break;
            default:
                drawableResourceId = R.drawable.stark_icon;
        }
        Picasso.with(DataManager.getInstance().getContext())
                .load(drawableResourceId)
                .into(holder.mHouseIcon);
    }

    @Override
    public int getItemCount() {
        return mCharacters.size();
    }


    public static class CharacterViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        protected ImageView mHouseIcon;
        protected TextView mCharacterName, mCharacterAliases;
        private CustomClickListener mListener;

        public CharacterViewHolder(View itemView, CustomClickListener customClickListener) {
            super(itemView);
            mHouseIcon = (ImageView)itemView.findViewById(R.id.house_icon);
            mCharacterName = (TextView)itemView.findViewById(R.id.character_name);
            mCharacterAliases = (TextView)itemView.findViewById(R.id.character_aliases);
            mListener = customClickListener;
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if(mListener != null){
                mListener.onCharacterItemClickListener(getAdapterPosition());
            }
        }

        public interface CustomClickListener{
            void onCharacterItemClickListener(int position);
        }
    }
}
