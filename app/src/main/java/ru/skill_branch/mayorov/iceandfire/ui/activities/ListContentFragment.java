package ru.skill_branch.mayorov.iceandfire.ui.activities;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import ru.skill_branch.mayorov.iceandfire.R;
import ru.skill_branch.mayorov.iceandfire.data.managers.DataManager;
import ru.skill_branch.mayorov.iceandfire.data.managers.models.CharacterInfo;
import ru.skill_branch.mayorov.iceandfire.data.storage.models.Character;
import ru.skill_branch.mayorov.iceandfire.ui.adapters.CharacterAdapter;
import ru.skill_branch.mayorov.iceandfire.utils.ConstantManager;


public class ListContentFragment extends Fragment implements CharacterAdapter.CharacterViewHolder.CustomClickListener
{
    private int mHouseId;
    private List<Character> mCharacterList;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mHouseId = getArguments().getInt(ConstantManager.HOUSE_ARG_ID);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        RecyclerView recyclerView = (RecyclerView)inflater.inflate(R.layout.recycle_view, container,false);

        mCharacterList = DataManager.getInstance().getCharacterListFromDb(mHouseId);

        CharacterAdapter adapter = new CharacterAdapter(mCharacterList,  mHouseId, this);
        recyclerView.setAdapter(adapter);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        return recyclerView;
    }

    @Override
    public void onCharacterItemClickListener(int position) {
        CharacterInfo characterInfo = DataManager.getInstance()
                .getCharacterInfoFromDb(mCharacterList.get(position).getRemoteId());

        if(characterInfo != null){
            startActivity(CharacterActivity.newIntent(getActivity(),characterInfo));
        }
        else{
            //TODO нет данных
        }
    }

}
