package ru.skill_branch.mayorov.iceandfire.mvp.models;

import ru.skill_branch.mayorov.iceandfire.data.managers.DataManager;
import ru.skill_branch.mayorov.iceandfire.data.managers.models.CharacterInfo;


public class CharacterModel {

    public CharacterInfo getCharacterInfo(Integer id){
        return DataManager.getInstance().getCharacterInfoFromDb(id);
    }
}
