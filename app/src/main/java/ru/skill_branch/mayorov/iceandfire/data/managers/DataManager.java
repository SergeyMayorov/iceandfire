package ru.skill_branch.mayorov.iceandfire.data.managers;

import android.content.Context;
import android.database.Cursor;
import android.support.annotation.Nullable;


import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import ru.skill_branch.mayorov.iceandfire.data.managers.models.CharacterInfo;
import ru.skill_branch.mayorov.iceandfire.data.network.api.RestService;
import ru.skill_branch.mayorov.iceandfire.data.network.ServiceGenerator;
import ru.skill_branch.mayorov.iceandfire.data.network.res.CharacterModelRes;
import ru.skill_branch.mayorov.iceandfire.data.network.res.HouseModelRes;
import ru.skill_branch.mayorov.iceandfire.data.storage.models.Character;
import ru.skill_branch.mayorov.iceandfire.data.storage.models.CharacterDao;
import ru.skill_branch.mayorov.iceandfire.data.storage.models.DaoSession;
import ru.skill_branch.mayorov.iceandfire.data.storage.models.House;
import ru.skill_branch.mayorov.iceandfire.data.storage.models.HouseDao;
import ru.skill_branch.mayorov.iceandfire.utils.StartApplication;


public class DataManager {
    private static DataManager INSTANCE = null;

    private Context mContext;
    private RestService mRestService;
    private DaoSession mDaoSession;


    private PreferencesManager mPreferencesManager;

    private DataManager() {
        this.mContext = StartApplication.getContext();
        this.mRestService = ServiceGenerator.createService(RestService.class);
        this.mDaoSession = StartApplication.getDaoSession();
        this.mPreferencesManager = new PreferencesManager();
    }

    public static DataManager getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new DataManager();
        }
        return INSTANCE;
    }

    public PreferencesManager getPreferencesManager() {
        return mPreferencesManager;
    }

    public DaoSession getDaoSession() {
        return mDaoSession;
    }

    public Context getContext() {
        return mContext;
    }

    //region ======= Network =======
    public Call<HouseModelRes> getHouseInfoFromNetwork(int houseId) {
        return mRestService.getHouseInfo(houseId);
    }

    public Call<CharacterModelRes> getCharacterFromNetwork(int characterId) {
        return mRestService.getCharacter(characterId);
    }
    //endregion

    //region ======= Database =======
    public List<Character> getCharacterListFromDb(Integer houseId) {
        List<Character> characterList = new ArrayList<>();
        try {
            characterList = mDaoSession.queryBuilder(Character.class)
                    .where(CharacterDao.Properties.HouseRemoteId.eq(houseId))
                    .orderAsc(CharacterDao.Properties.Name)
                    .build()
                    .list();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return characterList;
    }

    @Nullable
    public CharacterInfo getCharacterInfoFromDb(Integer remoteId) {
        CharacterInfo characterInfo = null;
        Cursor cursor = null;
        try {

            String sql = "SELECT \n" +
                    "c." + CharacterDao.Properties.Name.columnName + ", \n" +
                    "c." + CharacterDao.Properties.Titles.columnName + ",\n" +
                    "c." + CharacterDao.Properties.Aliases.columnName + ",\n" +
                    "c." + CharacterDao.Properties.Born.columnName + ",\n" +
                    "CASE WHEN c." + CharacterDao.Properties.Died.columnName + " IS NULL OR c." + CharacterDao.Properties.Died.columnName + " = '' THEN 0\n" +
                    "ELSE 1 END IsDead,\n" +
                    "m." + CharacterDao.Properties.RemoteId.columnName + " RemoteMotherId,\n" +
                    "m." + CharacterDao.Properties.Name.columnName + " MotherName,\n" +
                    "f." + CharacterDao.Properties.RemoteId.columnName + " RemoteFatherId,\n" +
                    "f." + CharacterDao.Properties.Name.columnName + " FatherName , \n" +
                    "h." + HouseDao.Properties.Words.columnName + ",\n" +
                    "h." + HouseDao.Properties.RemoteId.columnName + " HouseId\n" +
                    "FROM " + CharacterDao.TABLENAME + " c LEFT JOIN " + CharacterDao.TABLENAME + " f ON c." + CharacterDao.Properties.RemoteFatherId.columnName + "  = f." + CharacterDao.Properties.RemoteId.columnName + "\n" +
                    "LEFT JOIN " + CharacterDao.TABLENAME + " m ON c." + CharacterDao.Properties.RemoteMotherId.columnName + " = m." + CharacterDao.Properties.RemoteId.columnName + " \n" +
                    "JOIN " + HouseDao.TABLENAME + " h ON c." + CharacterDao.Properties.HouseRemoteId.columnName + " = h." + HouseDao.Properties.RemoteId.columnName + "\n" +
                    "WHERE c." + CharacterDao.Properties.RemoteId.columnName + " = ?;";

            cursor = mDaoSession.getDatabase().rawQuery(sql, new String[]{remoteId + ""});

            if (cursor.moveToFirst()) {
                return new CharacterInfo(
                        cursor.getString(cursor.getColumnIndex(CharacterDao.Properties.Name.columnName)),
                        cursor.getString(cursor.getColumnIndex(CharacterDao.Properties.Titles.columnName)),
                        cursor.getString(cursor.getColumnIndex(CharacterDao.Properties.Aliases.columnName)),
                        cursor.getString(cursor.getColumnIndex(CharacterDao.Properties.Born.columnName)),
                        cursor.getInt(cursor.getColumnIndex("IsDead")) == 1,
                        cursor.getInt(cursor.getColumnIndex("RemoteMotherId")),
                        cursor.getString(cursor.getColumnIndex("MotherName")),
                        cursor.getInt(cursor.getColumnIndex("RemoteFatherId")),
                        cursor.getString(cursor.getColumnIndex("FatherName")),
                        cursor.getString(cursor.getColumnIndex(HouseDao.Properties.Words.columnName)),
                        cursor.getInt(cursor.getColumnIndex("HouseId"))
                );
            }

//
//            Character character = mDaoSession.queryBuilder(Character.class)
//                    .where(CharacterDao.Properties.RemoteId.eq(remoteId))
//                    .build()
//                    .unique();
//            String motherName = null;
//
//            if (character.getRemoteMotherId() != null) {
//                Character mother = mDaoSession.queryBuilder(Character.class)
//                        .where(CharacterDao.Properties.RemoteId.eq(character.getRemoteMotherId()))
//                        .build()
//                        .unique();
//                if (mother != null) {
//                    motherName = mother.getName();
//                }
//            }
//
//            String fatherName = null;
//            if (character.getRemoteFatherId() != null) {
//                Character father = mDaoSession.queryBuilder(Character.class)
//                        .where(CharacterDao.Properties.RemoteId.eq(character.getRemoteFatherId()))
//                        .build()
//                        .unique();
//                if (father != null) {
//                    fatherName = father.getName();
//                }
//            }
//
//            House house = mDaoSession.queryBuilder(House.class)
//                    .where(HouseDao.Properties.RemoteId.eq(character.getHouseRemoteId()))
//                    .build()
//                    .unique();
//
//            characterInfo = new CharacterInfo(character, house.getWords(), motherName, fatherName);

        } catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            if(cursor != null && !cursor.isClosed()){
                cursor.close();
            }
        }
        return characterInfo;
    }

    public boolean isHouseDownloaded(Integer remoteHouseId) {

        House house = mDaoSession.queryBuilder(House.class)
                .where(HouseDao.Properties.RemoteId.eq(remoteHouseId))
                .build()
                .unique();
        return house != null;

    }

    //endregion

}
