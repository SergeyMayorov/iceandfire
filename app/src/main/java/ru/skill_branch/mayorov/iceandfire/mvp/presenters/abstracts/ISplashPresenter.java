package ru.skill_branch.mayorov.iceandfire.mvp.presenters.abstracts;


import ru.skill_branch.mayorov.iceandfire.data.network.DataLoaderOperation;


public interface ISplashPresenter  {
    void startLoadData();
    void finishLoadData(DataLoaderOperation.Result result);
}
