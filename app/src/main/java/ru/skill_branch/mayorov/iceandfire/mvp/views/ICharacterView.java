package ru.skill_branch.mayorov.iceandfire.mvp.views;

import ru.skill_branch.mayorov.iceandfire.data.managers.models.CharacterInfo;
import ru.skill_branch.mayorov.iceandfire.mvp.presenters.abstracts.ICharacterPresenter;



public interface ICharacterView extends IBaseView<ICharacterPresenter>{
    void startCharacterActivity(CharacterInfo characterInfo);

    void startCharacterListActivity();

    void hideFather();

    void hideMother();

    void loadImage(int resourceId);
}
