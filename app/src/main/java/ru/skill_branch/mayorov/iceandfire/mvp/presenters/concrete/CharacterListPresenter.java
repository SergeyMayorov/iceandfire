package ru.skill_branch.mayorov.iceandfire.mvp.presenters.concrete;

import android.view.MenuItem;

import ru.skill_branch.mayorov.iceandfire.R;
import ru.skill_branch.mayorov.iceandfire.mvp.presenters.abstracts.BasePresenter;
import ru.skill_branch.mayorov.iceandfire.mvp.presenters.abstracts.ICharacterListPresenter;
import ru.skill_branch.mayorov.iceandfire.mvp.views.ICharacterListView;


public class CharacterListPresenter extends BasePresenter<ICharacterListView> implements ICharacterListPresenter {
    private static CharacterListPresenter instance = new CharacterListPresenter();

    private CharacterListPresenter() {
    }

    @Override
    public void onNavigationHomeButtonClick() {
        if (getView() != null) {
            getView().showDrawer();
        }
    }

    @Override
    public void onNavigationDrawerItemSelected(MenuItem item) {
        if (getView() != null) {
            switch (item.getItemId()) {
                case R.id.house_starks:
                    getView().selectTabLayout(0);
                    break;
                case R.id.house_lannisters:
                    getView().selectTabLayout(1);
                    break;
                case R.id.house_targaryens:
                    getView().selectTabLayout(2);
                    break;
            }
            getView().hideDrawer();
        }
    }

    @Override
    public void onTabSelected(int tabPosition) {
        if (getView() != null) {
            switch (tabPosition) {
                case 0:
                    getView().setNavigationItemChecked(R.id.house_starks);
                    break;
                case 1:
                    getView().setNavigationItemChecked(R.id.house_lannisters);
                    break;
                case 2:
                    getView().setNavigationItemChecked(R.id.house_targaryens);
                    break;
            }
        }
    }

    public static CharacterListPresenter getInstance() {
        return instance;
    }
}
