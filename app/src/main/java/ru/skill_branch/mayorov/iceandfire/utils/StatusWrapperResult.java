package ru.skill_branch.mayorov.iceandfire.utils;

import android.support.annotation.Nullable;



public class StatusWrapperResult {

    private boolean mIsSuccessful;

    private String mMessage;

    public StatusWrapperResult(boolean isSuccessful){
        this(isSuccessful, null);
    }


    public StatusWrapperResult(boolean isSuccessful, String errorMessage){
        mIsSuccessful = isSuccessful;
        mMessage = errorMessage;
    }

    public boolean isSuccessful() {
        return mIsSuccessful;
    }

    @Nullable
    public String getMessage() {
        return mMessage;
    }
}
