package ru.skill_branch.mayorov.iceandfire.mvp.views;



public interface IBaseView<T> {
    void showMessage(String message);

    T getPresenter();
}
