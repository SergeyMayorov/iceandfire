package ru.skill_branch.mayorov.iceandfire.mvp.views;


import ru.skill_branch.mayorov.iceandfire.mvp.presenters.abstracts.ICharacterListPresenter;


public interface ICharacterListView extends IBaseView<ICharacterListPresenter> {

    void showDrawer();

    void hideDrawer();

    void selectTabLayout(int tabId);

    void setNavigationItemChecked(int resId);

}
