package ru.skill_branch.mayorov.iceandfire.ui.activities;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.skill_branch.mayorov.iceandfire.R;
import ru.skill_branch.mayorov.iceandfire.mvp.presenters.abstracts.ICharacterListPresenter;
import ru.skill_branch.mayorov.iceandfire.mvp.presenters.concrete.CharacterListPresenter;
import ru.skill_branch.mayorov.iceandfire.mvp.views.ICharacterListView;
import ru.skill_branch.mayorov.iceandfire.ui.adapters.PagerAdapter;
import ru.skill_branch.mayorov.iceandfire.utils.ConstantManager;

public class CharacterListActivity extends AppCompatActivity implements ICharacterListView, NavigationView.OnNavigationItemSelectedListener {

    private CharacterListPresenter mPresenter = CharacterListPresenter.getInstance();

    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    @BindView(R.id.tabs)
    TabLayout mTabLayout;

    @BindView(R.id.viewpager)
    ViewPager mViewPager;

    @BindView(R.id.drawer_layout)
    DrawerLayout mNavigationDrawer;

    @BindView(R.id.navigation_view)
    NavigationView mNavigationView;

    //region =========== Life Cycle ============
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_character_list);
        ButterKnife.bind(this);

        mPresenter.takeView(this);

        setupToolbar();
        setupViewPager();
        mNavigationView.setNavigationItemSelectedListener(this);
        mTabLayout.setupWithViewPager(mViewPager);

        mTabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                getPresenter().onTabSelected(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    @Override
    protected void onDestroy() {
        mPresenter.dropView();
        super.onDestroy();
    }
    //endregion

    //region ============ ICharacterListView ==================
    @Override
    public void showDrawer() {
        mNavigationDrawer.openDrawer(GravityCompat.START);
    }

    @Override
    public void hideDrawer() {
        mNavigationDrawer.closeDrawer(GravityCompat.START);
    }

    @Override
    public void selectTabLayout(int tabId) {
        mTabLayout.getTabAt(tabId).select();
    }

    @Override
    public void setNavigationItemChecked(int resId) {
        mNavigationView.setCheckedItem(resId);
    }


    @Override
    public void showMessage(String message) {

    }

    @Override
    public ICharacterListPresenter getPresenter() {
        return mPresenter;
    }
    //endregion

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            getPresenter().onNavigationHomeButtonClick();
        }
        return super.onOptionsItemSelected(item);
    }

    private void setupToolbar() {
        setSupportActionBar(mToolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setHomeAsUpIndicator(R.drawable.ic_menu_black_24dp);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    private void setupViewPager() {
        PagerAdapter adapter = new PagerAdapter(getSupportFragmentManager());
        adapter.addFragment(setupHouseFragment(ConstantManager.STARK_HOUSE_NUMBER), R.string.starks);
        adapter.addFragment(setupHouseFragment(ConstantManager.LANNISTER_HOUSE_NUMBER), R.string.lannisters);
        adapter.addFragment(setupHouseFragment(ConstantManager.TARGARYEN_HOUSE_NUMBER), R.string.targaryens);
        mViewPager.setAdapter(adapter);
    }


    private ListContentFragment setupHouseFragment(int houseId) {
        Bundle args = new Bundle();
        args.putInt(ConstantManager.HOUSE_ARG_ID, houseId);

        ListContentFragment fragment = new ListContentFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public static Intent newIntent(Context context) {
        Intent intent = new Intent(context, CharacterListActivity.class);
        return intent;
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        getPresenter().onNavigationDrawerItemSelected(item);
        return true;
    }
}
