package ru.skill_branch.mayorov.iceandfire.data.storage.models;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.NotNull;
import org.greenrobot.greendao.annotation.ToOne;
import org.greenrobot.greendao.annotation.Unique;



import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.DaoException;

import ru.skill_branch.mayorov.iceandfire.data.network.res.CharacterModelRes;

@Entity(active = true, nameInDb = "CHARACTERS")
public class Character {


    public Character(CharacterModelRes characterRes,
                     Integer remoteCharacterId,
                     Integer houseRemoteId,
                     Integer remoteFatherId,
                     Integer remoteMotherId,
                     String aliases,
                     String titles
    ){
        this.remoteId = remoteCharacterId;
        this.houseRemoteId = houseRemoteId;
        this.name = characterRes.getName();
        this.born = characterRes.getBorn();
        this.died = characterRes.getDied();
        this.remoteFatherId = remoteFatherId;
        this.remoteMotherId = remoteMotherId;
        this.aliases = aliases;
        this.titles = titles;
    }


    @Id
    private Long id;

    @NotNull
    @Unique
    private Integer remoteId;

    private Integer houseRemoteId;

    private String name;

    private String born;

    private String died;

    private Integer remoteFatherId;

    private Integer remoteMotherId;

    private String aliases;

    private String titles; //звание

    /** Used to resolve relations */
    @Generated(hash = 2040040024)
    private transient DaoSession daoSession;

    /** Used for active entity operations. */
    @Generated(hash = 898307126)
    private transient CharacterDao myDao;

    @Generated(hash = 17256763)
    public Character(Long id, @NotNull Integer remoteId, Integer houseRemoteId,
            String name, String born, String died, Integer remoteFatherId,
            Integer remoteMotherId, String aliases, String titles) {
        this.id = id;
        this.remoteId = remoteId;
        this.houseRemoteId = houseRemoteId;
        this.name = name;
        this.born = born;
        this.died = died;
        this.remoteFatherId = remoteFatherId;
        this.remoteMotherId = remoteMotherId;
        this.aliases = aliases;
        this.titles = titles;
    }

    @Generated(hash = 1853959157)
    public Character( ) {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getRemoteId() {
        return this.remoteId;
    }

    public void setRemoteId(Integer remoteId) {
        this.remoteId = remoteId;
    }

    public Integer getHouseRemoteId() {
        return this.houseRemoteId;
    }

    public void setHouseRemoteId(Integer houseRemoteId) {
        this.houseRemoteId = houseRemoteId;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBorn() {
        return this.born;
    }

    public void setBorn(String born) {
        this.born = born;
    }

    public String getDied() {
        return this.died;
    }

    public void setDied(String died) {
        this.died = died;
    }

    public Integer getRemoteFatherId() {
        return this.remoteFatherId;
    }

    public void setRemoteFatherId(Integer remoteFatherId) {
        this.remoteFatherId = remoteFatherId;
    }

    public Integer getRemoteMotherId() {
        return this.remoteMotherId;
    }

    public void setRemoteMotherId(Integer remoteMotherId) {
        this.remoteMotherId = remoteMotherId;
    }

    public String getAliases() {
        return this.aliases;
    }

    public void setAliases(String aliases) {
        this.aliases = aliases;
    }

    public String getTitles() {
        return this.titles;
    }

    public void setTitles(String titles) {
        this.titles = titles;
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#delete(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 128553479)
    public void delete() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.delete(this);
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#refresh(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 1942392019)
    public void refresh() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.refresh(this);
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#update(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 713229351)
    public void update() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.update(this);
    }

    /** called by internal mechanisms, do not call yourself. */
    @Generated(hash = 162219484)
    public void __setDaoSession(DaoSession daoSession) {
        this.daoSession = daoSession;
        myDao = daoSession != null ? daoSession.getCharacterDao() : null;
    }
}
