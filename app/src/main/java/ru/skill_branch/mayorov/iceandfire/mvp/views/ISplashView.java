package ru.skill_branch.mayorov.iceandfire.mvp.views;

import com.redmadrobot.chronos.gui.ChronosConnectorWrapper;

import ru.skill_branch.mayorov.iceandfire.mvp.presenters.abstracts.ISplashPresenter;


public interface ISplashView extends ChronosConnectorWrapper, IBaseView<ISplashPresenter> {
    void showProgress();
    void hideProgress();

    void startActivity();
    void finishActivity();
}
